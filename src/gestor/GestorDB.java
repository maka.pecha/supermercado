package gestor;

import dto.OfertaDTO;
import java.sql.*;
import java.util.ArrayList;
import modelo.*;
/**
 *
 * @author Maka
 */
public class GestorDB {
    private Connection con;
    private String CONN = "jdbc:sqlserver://localhost\\SQLEXPRESS:1434;databaseName=Supermercado";
    private String USER= "sa";
    private String PASS= "38161112";
    
    public void abrirConexion() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(CONN, USER, PASS);
        } catch (Exception exc){
            exc.printStackTrace();
        }
    }
    
    public void cerrarConexion(){
        try {
            if(con != null && !con.isClosed()){
                con.close();
            }
        } catch (Exception exc){
            exc.printStackTrace();
        }
    }
    
    public void agregarProducto(Producto p){
        
        try {
            
            abrirConexion();
            
            PreparedStatement ps = con.prepareStatement("INSERT INTO Producto VALUES (?)");
            ps.setString(1,p.getNombre());
            ps.executeUpdate();
            
            ps.close();
           
        } 
        catch (Exception exc) {
            exc.printStackTrace();
        }
       finally
        {
            cerrarConexion();
        }
    }
    
    public ArrayList<Producto> obtenerProductos(){
        ArrayList<Producto> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "SELECT * FROM Producto";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                Producto v = new Producto(id, nombre);
                lista.add(v);
            }
        
        } catch(Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
    public void agregarOferta(Oferta o){
        try{
            abrirConexion();
            
            String query = "INSERT INTO Oferta VALUES (?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareCall(query);
            ps.setInt(1, o.getProducto().getId());
            ps.setDouble(2, o.getPrecioNormal());
            ps.setDouble(3, o.getPrecioOferta());
            ps.setInt(4, o.getStockDisponible());
            ps.setString(5, o.getFechaInicioOferta());
            ps.setInt(6, o.getDiasVigencia());
            ps.executeUpdate();
            ps.close();
            
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally
        {
            cerrarConexion();
        }
    }
    
    public ArrayList<OfertaDTO> obtenerOfertasDTO(){
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "select o.id, p.nombre, o.precioNormal, o.precioOferta, o.stockDisponible, o.fechaInicioOferta, o.diasVigencia\n" +
                            "from Oferta o\n" +
                            "join Producto p on p.id = o.idProducto";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                double precioNormal = rs.getDouble("precioNormal");
                double precioOferta = rs.getDouble("precioOferta");
                int stockDisponible = rs.getInt("stockDisponible");
                String fechaInicioOferta = rs.getString("fechaInicioOferta");
                int diasVigencia = rs.getInt("diasVigencia");
                
                OfertaDTO o = new OfertaDTO(id, nombre, precioNormal, precioOferta, stockDisponible, fechaInicioOferta, diasVigencia);
                lista.add(o);
            }
            rs.close();
            st.close();
            
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
    public int cantidadProductosOfertadosMas5Dias(){
        int cantidad = 0;
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            ResultSet rs =st.executeQuery("SELECT sum(o.stockDisponible) as cantidad\n" +
                                            "from Oferta o \n" +
                                            "where o.diasVigencia > 5");
            if(rs.next()) {
                cantidad = rs.getInt(1);
            }
            rs.close();
            st.close();
            } 
        catch (Exception exc) {
                exc.printStackTrace();
            }
        finally{
                cerrarConexion();
            }
        return cantidad;
    }
    
    public double obtenerMontoPorOferta(int idProducto){
      double monto  = 0;
      
      try {
            abrirConexion();

            String sql = "SELECT sum((o.precioNormal - o.precioOferta) * o.stockDisponible) as monto\n" +
                          "from Oferta o \n" +
                          "where o.idProducto = ?";
            PreparedStatement pt = con.prepareStatement(sql);
            pt.setInt(1, idProducto);
            
            ResultSet rs = pt.executeQuery(); // usa prepareStatement con executeQuery para busqueda con filtro
            
            if(rs.next()){
                monto = rs.getDouble("monto");
            }
            
            rs.close();
            pt.close();
        } 
         catch (Exception exc) {
          exc.printStackTrace();
        }
        finally
        {
            cerrarConexion();
        }
        return monto;
    }
    
}
