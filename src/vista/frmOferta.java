/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import gestor.GestorDB;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import modelo.*;
import dto.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Maka
 */
public class frmOferta extends javax.swing.JFrame {
    private frmReportes ventanaReporte;
    private frmListado ventanaListado;
    private frmProducto ventanaProducto;
    GestorDB gestor = new GestorDB();

    /**
     * Creates new form frmOferta
     */
    public frmOferta() {
        initComponents();
        cargarCombo();
    }
    
    public void cargarCombo(){
        ArrayList<Producto> productos = gestor.obtenerProductos();
        DefaultComboBoxModel modeloProducto = new DefaultComboBoxModel();
        for (Producto p : productos) {
            modeloProducto.addElement(p);
        }
        cboProducto.setModel(modeloProducto);
    }
    
    public boolean validation(){
        if (txtPrecioNormal.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "El precio normal debe agregarse");
            txtPrecioNormal.requestFocus();
            return false;
        }
        if (txtPrecioOferta.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "El precio en oferta debe agregarse");
            txtPrecioOferta.requestFocus();
            return false;
        }
        if (txtStockDisponible.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "El stock disponible debe agregarse");
            txtStockDisponible.requestFocus();
            return false;
        }
        if (txtFechaInicioOferta.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "La fecha debe agregarse");
            txtFechaInicioOferta.requestFocus();
            return false;
        }
        if (txtDiasVigencia.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "Los días de vigencia deben agregarse");
            txtDiasVigencia.requestFocus();
            return false;
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cboProducto = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        txtPrecioNormal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPrecioOferta = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtStockDisponible = new javax.swing.JTextField();
        txtFechaInicioOferta = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtDiasVigencia = new javax.swing.JTextField();
        btnAgregarOferta = new javax.swing.JButton();
        btnListado = new javax.swing.JButton();
        btnReportes = new javax.swing.JButton();
        btnNuevoProducto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Carga de Ofertas");
        setBackground(new java.awt.Color(0, 204, 204));

        jLabel1.setFont(new java.awt.Font("Courier New", 1, 24)); // NOI18N
        jLabel1.setText("Ofertas");

        jLabel2.setText("Producto");

        jLabel3.setText("Precio Normal");

        txtPrecioNormal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPrecioNormalFocusLost(evt);
            }
        });

        jLabel4.setText("Precio Oferta");

        txtPrecioOferta.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPrecioOfertaFocusLost(evt);
            }
        });

        jLabel5.setText("Stock Disponible");

        txtStockDisponible.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtStockDisponibleFocusLost(evt);
            }
        });

        jLabel6.setText("Fecha de inicio de Oferta");

        jLabel7.setText("Días de Vigencia");

        txtDiasVigencia.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDiasVigenciaFocusLost(evt);
            }
        });

        btnAgregarOferta.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnAgregarOferta.setText("Agregar Oferta");
        btnAgregarOferta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarOfertaActionPerformed(evt);
            }
        });

        btnListado.setText("Listado");
        btnListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListadoActionPerformed(evt);
            }
        });

        btnReportes.setText("Reportes");
        btnReportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportesActionPerformed(evt);
            }
        });

        btnNuevoProducto.setText("Agregar Producto");
        btnNuevoProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoProductoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabel7))
                                        .addGap(52, 52, 52)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(cboProducto, 0, 152, Short.MAX_VALUE)
                                            .addComponent(txtPrecioNormal)
                                            .addComponent(txtPrecioOferta)
                                            .addComponent(txtStockDisponible)
                                            .addComponent(txtFechaInicioOferta)
                                            .addComponent(txtDiasVigencia)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnListado)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnReportes))
                                    .addComponent(btnNuevoProducto)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(130, 130, 130)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 30, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAgregarOferta)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cboProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtPrecioNormal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtPrecioOferta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtStockDisponible, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtFechaInicioOferta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtDiasVigencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarOferta)
                    .addComponent(btnNuevoProducto))
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnListado)
                    .addComponent(btnReportes))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarOfertaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarOfertaActionPerformed
        if (validation()) {
            Producto producto = (Producto)cboProducto.getSelectedItem();
            Double precioNormal = Double.parseDouble(txtPrecioNormal.getText());
            Double precioOferta = Double.parseDouble(txtPrecioOferta.getText());
            int stockDisponible = Integer.parseInt(txtStockDisponible.getText());
            String fechaInicioOferta = txtFechaInicioOferta.getText();
            int diasVigencia = Integer.parseInt(txtDiasVigencia.getText());

            Oferta o = new Oferta(0, producto, precioNormal, precioOferta, stockDisponible, fechaInicioOferta, diasVigencia);
            GestorDB gestor = new GestorDB();
            gestor.agregarOferta(o);
            
            JOptionPane.showMessageDialog(null, "Registro exitoso!");

            txtPrecioNormal.setText("");
            txtPrecioOferta.setText("");
            txtStockDisponible.setText("");
            txtFechaInicioOferta.setText("");
            txtDiasVigencia.setText("");
            if (ventanaListado != null){
                ventanaListado.cargarTabla();
            }
        }
    }//GEN-LAST:event_btnAgregarOfertaActionPerformed

    private void btnListadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListadoActionPerformed
        if(ventanaListado == null){
            ventanaListado = new frmListado();
        }
        ventanaListado.setVisible(true);
    }//GEN-LAST:event_btnListadoActionPerformed

    private void btnReportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportesActionPerformed
        if(ventanaReporte == null){
            ventanaReporte = new frmReportes();
        }
        ventanaReporte.setVisible(true);
    }//GEN-LAST:event_btnReportesActionPerformed

    private void txtPrecioNormalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrecioNormalFocusLost
        String text = txtPrecioNormal.getText();
        try {
            double x = Double.parseDouble(text);
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "El precio normal debe ser un número");
            txtPrecioNormal.requestFocus();
        }
    }//GEN-LAST:event_txtPrecioNormalFocusLost

    private void txtPrecioOfertaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrecioOfertaFocusLost
        String text = txtPrecioOferta.getText();
        try {
            double x = Double.parseDouble(text);
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "El precio de oferta debe ser un número");
            txtPrecioOferta.requestFocus();
        }
    }//GEN-LAST:event_txtPrecioOfertaFocusLost

    private void txtStockDisponibleFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtStockDisponibleFocusLost
        String text = txtStockDisponible.getText();
        try {
            int x = Integer.parseInt(text);
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "El stock debe ser un número");
            txtStockDisponible.requestFocus();
        }
    }//GEN-LAST:event_txtStockDisponibleFocusLost

    private void txtDiasVigenciaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDiasVigenciaFocusLost
        String text = txtDiasVigencia.getText();
        try {
            int x = Integer.parseInt(text);
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Los días deben ser un número");
            txtDiasVigencia.requestFocus();
        }
    }//GEN-LAST:event_txtDiasVigenciaFocusLost

    private void btnNuevoProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoProductoActionPerformed
        if(ventanaProducto == null){
            ventanaProducto = new frmProducto();
        }
        ventanaProducto.setVisible(true);
    }//GEN-LAST:event_btnNuevoProductoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmOferta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmOferta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmOferta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmOferta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmOferta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarOferta;
    private javax.swing.JButton btnListado;
    private javax.swing.JButton btnNuevoProducto;
    private javax.swing.JButton btnReportes;
    private javax.swing.JComboBox<Producto> cboProducto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField txtDiasVigencia;
    private javax.swing.JTextField txtFechaInicioOferta;
    private javax.swing.JTextField txtPrecioNormal;
    private javax.swing.JTextField txtPrecioOferta;
    private javax.swing.JTextField txtStockDisponible;
    // End of variables declaration//GEN-END:variables
}
